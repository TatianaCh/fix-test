import * as ActionTypes from '../constants';

function signIn(data) {
  const login = data.login || '';
  return {
    type: ActionTypes.SIGN_IN,
    payload: { login },
  };
}

export default {
  signIn,
};

