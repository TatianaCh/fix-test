import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import icons from '../images/svg-icons';
import Login from './Login';
import MainPage from './MainPage';
import Calculations from './Calculations';
import PrivateRouter from './PrivateRouter';

class App extends Component {
  render() {
    return (
    <Provider store={this.props.store}>
      <BrowserRouter>
        <div className="root">
          <div className="svgContainer" dangerouslySetInnerHTML={{ __html: icons }} />
          <Route exact path="/" component={MainPage} />
          <Route exact path="/login" component={Login} />
          <PrivateRouter exact path="/operation" component={Calculations} />
        </div>
      </BrowserRouter>
    </Provider>
    );
  }
}

export default App;
