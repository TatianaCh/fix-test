import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import actions from '../actions';
import LoginForm from '../components/LoginForm';
import './Login.scss';

class Login extends Component {
  static propTypes = {
    profile: PropTypes.object.isRequired,
    signIn: PropTypes.func.isRequired,
  };

  componentDidMount() {
    document.title = 'Логин';
  }

  render() {
    const { profile, signIn } = this.props;
    if (profile.login) {
      return <Redirect to="/operation" />;
    }
    return (
      <div className="page-content login-page">
        <h2>Вход</h2>
        <LoginForm onSubmit={signIn} />
      </div>
    );
  }
}


const mapStateToProps = state => ({
  ...state,
});

const mapActions = {
  ...actions,
};

const LoginHOC = connect(mapStateToProps, mapActions)(Login);

export default LoginHOC;
