import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect, Route, withRouter } from 'react-router-dom';

class PrivateRouter extends Component {
  static propTypes = {
    profile: PropTypes.object.isRequired,
  };

  render() {
    const { profile, component, ...rest } = this.props;
    return (
      <Route {...rest} render={props => (profile.login ? (<this.props.component {...props}/>) : (<Redirect to='/login' />))}/>
    );
  }
}

const mapStateToProps = state => ({
  ...state,
});

export default withRouter(connect(mapStateToProps)(PrivateRouter));
