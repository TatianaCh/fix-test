import React from 'react';
import { Link } from 'react-router-dom';
import './MainPage.scss';

export default () => (
  <div className="page-content main-page">
    <h1>
      Приложение для вычисления суммы чисел.
    </h1>
    <div className="description">
      Пожалуйста, войдите в систему, чтобы продолжить!
      <Link to='/login'>Войти</Link>
    </div>
  </div>
);