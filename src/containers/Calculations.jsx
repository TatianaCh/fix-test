import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ProgressLoader from '../components/ProgressLoader';
import Step1 from '../components/Step1';
import Step2 from '../components/Step2';
import Step4 from '../components/Step4';
import './Calculations.scss';

class Calculations extends Component {
  static propTypes = {
    profile: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.finishInputHandler = this.finishInputHandler.bind(this);
    this.startCounting = this.startCounting.bind(this);
    this.userName = this.formatLogin(props.profile.login);
    this.state = {
      step: 1,
      data: [],
    };
  }

  componentDidMount() {
    document.title = 'Сложение';
  }

  finishInputHandler(data) {
    this.setState({
      data,
    });
    this.onChangeStep(2)();
  }

  startCounting() {
    this.onChangeStep(3)();
    setTimeout(() => {
      this.onChangeStep(4)();
    }, 5000);
  }

  onChangeStep = step => () => {
    if (step > 0 && step < 5) {
      const prevStep = this.state.step;
      this.stepContainer.className = prevStep < step ? 'toLeft' : 'toRight';
      setTimeout(() => {
        this.setState({
          step,
        }, () => {
          this.stepContainer.className = prevStep < step ? 'fromRight' : 'fromLeft';
        });
      }, 300);
    }
  }

  formatLogin(email) {
    const emailParts = email.split(/[@.]/);
    let domainPart = emailParts[1];
    const domainLength = domainPart.length;
    if (domainLength > 1) {
      domainPart = domainPart.substr(0, 1) + new Array(domainLength).join('*');
    }
    const userNamePart = emailParts[0].charAt(0).toUpperCase() + emailParts[0].substr(1, emailParts[0].length);
    const emailText = `${userNamePart || ''}@${domainPart || ''}.${emailParts[2] || ''}`;
    return emailText;
  }

  render() {
    const { step, data } = this.state;
    let mainComponent;
    switch (step) {
      case 2: mainComponent = (<Step2 data={data} next={this.startCounting} prev={this.onChangeStep(1)} />);
        break;
      case 3: mainComponent = (
          <div className="loading-page">
            <h2>Расчет</h2>
            <ProgressLoader />
          </div>
        );
        break;
      case 4: mainComponent = (<Step4 data={data} prev={this.onChangeStep(1)} />);
        break;
      default: mainComponent = (<Step1 next={this.finishInputHandler} />)
    }

    return (
      <div className="page-content operation-page">
        <aside>
          <div className="welcome-message">Привет, {this.userName}!</div>
          <div>
            Сложение (прибавление) — одна из основных операций (действий) в разных разделах математики, 
            позволяющая объединить два объекта (в простейшем случае — два числа). Более строго сложение — бинарная операция, 
            определённая на некотором множестве, элементы которого мы будем называть числами, при которой двум числовым аргументам (слагаемым)
             a и b сопоставляется итог (сумма), обычно обозначаемый с помощью знака «плюс»: a+b.
          </div>
        </aside>
        <main>
          <div ref={(el) => { this.stepContainer = el; }}>
            {mainComponent}
          </div>
        </main>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  ...state,
});

const CalculationsHOC = connect(mapStateToProps)(Calculations);

export default CalculationsHOC;
