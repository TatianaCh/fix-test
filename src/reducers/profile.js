import * as ActionTypes from '../constants';

const initialState = {};

export default function profile(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SIGN_IN:
      {
        return { ...state, ...action.payload };
      }
    default:
      return state;
  }
};
