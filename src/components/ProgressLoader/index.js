import React from 'react';
import loaderImage from '../../images/dual-loader.gif';


export default () => (
  <div className="loader">
    <img src={loaderImage} alt="" />
  </div>
);
