import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Step1.scss';


class Step1 extends Component {
  static propTypes = {
    next: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.onNextClick = this.onNextClick.bind(this);
    this.addValue = this.addValue.bind(this);

    this.newInput = null;
    const initialData = ['', ''];
    this.state = {
      data: initialData,
      isValid: this.checkValidation(initialData),
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.data.length > prevState.data.length && this.newInput) {
      this.newInput.focus();
    }
  }

  onChangeValue = index => (event) => {
    const data = [...this.state.data];
    const parsedValue = parseInt(event.target.value, 10);
    data[index] = isNaN(parsedValue) || parsedValue < 0 ? 0 : parsedValue;
    this.setState({
      data,
      isValid: this.checkValidation(data),
    });
  }

  addValue() {
    const data = [...this.state.data];
    data.push('');
    this.setState({
      data,
      isValid: this.checkValidation(data),
    });
  }

  deleteValue = index => () => {
    const data = [...this.state.data];
    data.splice(index, 1);
    this.setState({
      data,
      isValid: this.checkValidation(data),
    });
  }

  onNextClick() {
    this.props.next(this.state.data);
  }

  checkValidation = data => {
    return data.every(value => (!isNaN(parseInt(value, 10))));
  }

  render() {
    const { data, isValid } = this.state;
    const inputs = data.map((value, index) => {
      let operationSign = (<span>+</span>);
      const removeBtn = (
        <div className="input-btn" title="Delete" onClick={this.deleteValue(index)}>
          <svg>
            <use xlinkHref="#icon-minus" />
          </svg>
        </div>
      );
      const inputProps = {
        key: index,
        type: 'text',
        pattern: '[0-9]*',
        onChange: this.onChangeValue(index),
        value,
      };
      if (index === data.length - 1) {
        operationSign = (
          <div className="input-btn add-btn" title="Add" onClick={this.addValue}>
            <svg>
              <use xlinkHref="#icon-plus" />
            </svg>
          </div>
        );
        inputProps.ref = (el) => { this.newInput = el; };
      }
      return (
        <div key={index} className="operation-block">
          <div className="form-input">
            {data.length > 2 && removeBtn}
            <input {...inputProps} />
          </div>
          {operationSign}
        </div>
      );
    });

    return (
      <div className="form input-data">
        <h2>Ввод данных</h2>
        <div className="data-container">
          {inputs}
        </div>
        <div className="form-footer">
          <button onClick={this.onNextClick} className="next-btn" disabled={!isValid}>Продолжить</button>
        </div>
      </div>
    );
  }
}

export default Step1;
