import React, { Component } from 'react';
import PropTypes from 'prop-types';
import sortDirection from '../../constants/sortDirection';
import './Step2.scss';


class Step2 extends Component {
  static propTypes = {
    data: PropTypes.array,
    next: PropTypes.func.isRequired,
    prev: PropTypes.func.isRequired,
  };

  static get defaultProps() {
    return {
      data: [],
    };
  }

  constructor(props) {
    super(props);
    this.filter = this.filter.bind(this);
    this.sort = this.sort.bind(this);
    this.dataDictionary = props.data.map((value, index) => ({ value, index }));
    this.state = {
      data: [...this.dataDictionary],
      sort: null, 
    };
  }

  sort() {
    let sort = this.state.sort;
    const data = [...this.state.data];
    if (sort === sortDirection.asc) {
      sort = sortDirection.desc;
      data.sort((item1, item2) => (item2.value - item1.value));
    } else if (sort === sortDirection.desc) {
      sort = null;
      data.sort((item1, item2) => (item2.index - item1.index));
    } else {
      sort = sortDirection.asc;
      data.sort((item1, item2) => (item1.value - item2.value));
    }
    this.setState({
      data,
      sort,
    });
  }

  filter(event) {
    const { sort } = this.state;
    const value = event.target.value;
    const data = this.dataDictionary.filter((item) => (item.value.toString().indexOf(value) > -1));
    if (sort === sortDirection.asc) {
      data.sort((item1, item2) => (item2.value - item1.value));
    } else if (sort === sortDirection.desc) {
      data.sort((item1, item2) => (item1.value - item2.value));
    }
    this.setState({
      data,
    });
  }

  render() {
    const { data, sort } = this.state;
    const rows = data.map((item) => {
      return (
        <tr key={item.index}>
          <td>{item.index}</td>
          <td>{item.value}</td>
        </tr>
      );
    });
    let sortClass = 'sort-btn';
    if (sort === sortDirection.asc) {
      sortClass += ' asc-order';
    } else if (sort === sortDirection.desc) {
      sortClass += ' desc-order';
    }
    const sortBtn = (
      <div className={sortClass} onClick={this.sort}>
        <svg>
          <use xlinkHref="#icon-chevron-down" />
        </svg>
      </div>
    );
    return (
      <div className="form">
        <h2>Подтверждение данных</h2>
        <div className="form-input">
          <input type="text" className="search-input" onChange={this.filter} placeholder="Search" />
        </div>
        <div className="data-container">
          <table className="values-table">
            <thead>
              <tr>
                <th>#</th>
                <th>
                  <span>Число</span>
                  {sortBtn}
                </th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
        </div>
        <div className="form-footer">
          <button onClick={this.props.prev} className="prev-btn">Назад</button>
          <button onClick={this.props.next} className="next-btn">Продолжить</button>
        </div>
      </div>
    );
  }
}

export default Step2;
