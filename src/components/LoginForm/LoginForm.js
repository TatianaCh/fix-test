import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './LoginForm.scss';


class LoginForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.state = {
      login: '',
      password: '',
      isValid: false,
    };
  }

  onSubmitHandler() {
    const { login, password, isValid } = this.state;
    if (isValid) {
      this.props.onSubmit({ login, password });
    }
  }

  onChangeHandler() {
    if (this.loginInput && this.passwordInput) {
      const isValid = this.loginInput.validity.valid && this.passwordInput.validity.valid;
      this.setState({
        login: this.loginInput.value.substr(0, 250),
        password: this.passwordInput.value.substr(0, 250),
        isValid,
      });
    }
  }

  render() {
    const { login, password, isValid } = this.state;
    return (
      <div className="form login-form">
        <div className="description">
          Пожалуйста, введите логин и пароль! Необходимая длина пароля 6 знаков.
        </div>
        <div className="form-input">
          <input
            type="email"
            placeholder="Email"
            value={login}
            ref={(el) => { this.loginInput = el; }}
            onChange={this.onChangeHandler}
            required
          />
        </div>
        <div className="form-input">
          <input
            type="password"
            placeholder="Пароль"
            pattern=".{6,}"
            value={password}
            ref={(el) => { this.passwordInput = el; }}
            onChange={this.onChangeHandler}
            required
          />
        </div>
        <button type="submit" onClick={this.onSubmitHandler} disabled={!isValid}>Вход</button>
      </div>
    );
  }
}

export default LoginForm;
