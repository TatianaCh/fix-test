import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Step4.scss';


class Step4 extends Component {
  static propTypes = {
    data: PropTypes.array,
    prev: PropTypes.func.isRequired,
  };

  static get defaultProps() {
    return {
      data: [],
    };
  }

  render() {
    const { data, prev } = this.props;
    let sum = 0;
    const rows = data.map((value, index) => {
      sum += value;
      const rowClass = value > 10 ? 'big-value' : '';
      return (
        <tr key={index} className={rowClass}>
          <td>{index}</td>
          <td>{value}</td>
        </tr>
      );
    });
    return (
      <div className="form">
        <h2>Результат</h2>
        <p>Сумма = {sum}</p>
        <p>Слагаемые :</p>
        <div className="data-container">
          <table className="values-table">
            <thead>
              <tr>
                <th>#</th>
                <th>
                  Число
                </th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
        </div>
        <div className="form-footer">
          <button onClick={prev} className="prev-btn">Вернуться к списку данных</button>
        </div>
      </div>
    );
  }
}

export default Step4;
