import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './containers/App';
import configureStore from './store/configureStore';
import registerServiceWorker from './registerServiceWorker';

const store = configureStore();

ReactDOM.render(
  <App store={store} />,
  document.getElementById('root')
 );
registerServiceWorker();
